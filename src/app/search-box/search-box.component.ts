import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {
  @Input() placeholderText: string;
  @Input() contentText: string;
  @Output() searchEvent = new EventEmitter<string>();

  searchValue = '';

  constructor() { }

  ngOnInit() {

  }

   public clearInput() {
    this.searchValue = '';
    this.searchEvent.emit(this.contentText);
  }

  public searchAction() {
    if (this.searchValue !== '' ) {
      const pattern = new RegExp( this.searchValue, 'gi');
      const newText = this.contentText.replace(pattern, '<span class=\'highlight\'>' + this.searchValue + '</span>');
      console.log(newText);
      this.searchEvent.emit(newText);
    } else {
      this.searchEvent.emit(this.contentText);
    }
  }
}
